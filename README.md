# NSS05

## List of Authors
- Vanessa Bielena
- The Dark Lord

## List of Technologies
- Python
- Flask
- HTML
- CSS

## Description
This is a brief description of NSS05 the application.

## Installation
To install this application, follow these steps:
1. Clone the repository.
2. Navigate to the project directory.
3. Run `python setup.py install`.

## Copyright
© 2024 Vanessa Bielena and The Dark Lord. All rights reserved.

